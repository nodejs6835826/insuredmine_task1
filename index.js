const express = require('express');
const mongoose = require('mongoose');
const apiRoutes = require('./routes/api');

const app = express();

mongoose.connect('mongodb://localhost:27017/insurance_db', {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

app.use(express.json());
app.use('/api', apiRoutes);

app.listen(3000, () => {
    console.log('Server is running on port 3000');
});
