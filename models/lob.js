const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const LOBSchema = new Schema({
    category_name: { type: String, required: true }
});

module.exports = mongoose.model('LOB', LOBSchema);
