const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserAccountSchema = new Schema({
    account_name: { type: String, required: true }
});

module.exports = mongoose.model('UserAccount', UserAccountSchema);
