// models/Agent.js
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const AgentSchema = new Schema({
    name: String
});

module.exports = mongoose.model('Agent', AgentSchema);
