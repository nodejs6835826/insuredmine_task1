const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PolicySchema = new Schema({
    policy_number: { type: String, required: true },
    policy_start_date: { type: Date, required: true },
    policy_end_date: { type: Date, required: true },
    policy_category_id: { type: Schema.Types.ObjectId, ref: 'LOB', required: true },
    company_id: { type: Schema.Types.ObjectId, ref: 'Carrier', required: true },
    user_id: { type: Schema.Types.ObjectId, ref: 'User', required: true }
});

module.exports = mongoose.model('Policy', PolicySchema);
