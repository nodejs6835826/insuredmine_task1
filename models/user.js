const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
    first_name: { type: String, required: true },
    DOB: { type: Date, required: true },
    address: { type: String, required: true },
    phone_number: { type: String, required: true },
    state: { type: String, required: true },
    zip_code: { type: String, required: true },
    email: { type: String, required: true },
    gender: { type: String, required: true },
    userType: { type: String, required: true }
});

module.exports = mongoose.model('User', UserSchema);
