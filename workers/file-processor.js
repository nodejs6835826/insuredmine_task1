// workers/fileProcessor.js
const { parentPort, workerData } = require('worker_threads');
const mongoose = require('mongoose');
const fs = require('fs');
const path = require('path');
const csvParser = require('csv-parser');
const xlsx = require('xlsx');
const Agent = require('../models/agents');
const User = require('../models/user');
const UserAccount = require('../models/useraccount');
const LOB = require('../models/lob');
const Carrier = require('../models/carrier');
const Policy = require('../models/policy');

mongoose.connect('mongodb://localhost:27017/insurance_db', {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

const { filePath, collectionName } = workerData;

async function processCsvFile(filePath, collectionName) {
    return new Promise((resolve, reject) => {
        const records = [];
        fs.createReadStream(filePath)
            .pipe(csvParser())
            .on('data', (row) => records.push(row))
            .on('end', async () => {
                await insertData(collectionName, records);
                resolve();
            })
            .on('error', reject);
    });
}

async function processXlsxFile(filePath, collectionName) {
    return new Promise((resolve, reject) => {
        try {
            const workbook = xlsx.readFile(filePath);
            const sheetName = workbook.SheetNames[0];
            const data = xlsx.utils.sheet_to_json(workbook.Sheets[sheetName]);
            insertData(collectionName, data).then(resolve).catch(reject);
        } catch (err) {
            reject(err);
        }
    });
}

async function insertData(collectionName, data) {
    switch (collectionName) {
        case 'Agent':
            await Agent.insertMany(data);
            break;
        case 'User':
            await User.insertMany(data);
            break;
        case 'UserAccount':
            await UserAccount.insertMany(data);
            break;
        case 'LOB':
            await LOB.insertMany(data);
            break;
        case 'Carrier':
            await Carrier.insertMany(data);
            break;
        case 'Policy':
            await Policy.insertMany(data);
            break;
        default:
            throw new Error('Invalid collection name');
    }
}

(async () => {
    if (filePath.endsWith('.csv')) {
        await processCsvFile(filePath, collectionName);
    } else if (filePath.endsWith('.xlsx')) {
        await processXlsxFile(filePath, collectionName);
    }
    parentPort.postMessage({ message: 'File processed successfully' });
})();
