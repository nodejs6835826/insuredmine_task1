// routes/api.js
const express = require('express');
const multer = require('multer');
const path = require('path');
const { Worker } = require('worker_threads');
const User = require('./../models/user');
const Policy = require('./../models/policy');

const router = express.Router();
const upload = multer({ dest: 'uploads/' });

router.post('/upload', upload.single('file'), (req, res) => {
    const collectionName = req.body.collection;
    const filePath = path.join(__dirname, '..', req.file.path);

    const worker = new Worker(path.join(__dirname, '../workers/file-processor.js'), {
        workerData: { filePath, collectionName }
    });

    let responseSent = false;

    worker.on('message', (message) => {
        if (!responseSent) {
            res.json({ message: message.message });
            responseSent = true;
        }
    });

    worker.on('error', (error) => {
        if (!responseSent) {
            res.status(500).json({ message: "Error processing file", error: error.message });
            responseSent = true;
        }
    });

    worker.on('exit', (code) => {
        if (!responseSent) {
            if (code !== 0) {
                res.status(500).json({ message: `Worker stopped with exit code ${code}` });
            } else {
                res.json({ message: 'File processed successfully' });
            }
            responseSent = true;
        }
    });
});

router.get('/search_policy', async (req, res) => {
    const username = req.query.username;
    const user = await User.findOne({ first_name: username });
    if (!user) {
        return res.status(404).json({ message: "User not found" });
    }
    const policies = await Policy.find({ user_id: user._id });
    res.json(policies);
});

router.get('/aggregate_policies', async (req, res) => {
    const aggregateData = await Policy.aggregate([
        {
            $lookup: {
                from: 'users',
                localField: 'user_id',
                foreignField: '_id',
                as: 'user_info'
            }
        },
        {
            $unwind: '$user_info'
        },
        {
            $group: {
                _id: '$user_info.first_name',
                total_policies: { $sum: 1 },
                policies: { $push: '$$ROOT' }
            }
        }
    ]);
    res.json(aggregateData);
});

module.exports = router;
